# install node for coc
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt install nodejs

# install neovim
wget --quiet https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage --output-document nvim
chmod +x nvim
sudo mv nvim /usr/bin

# clone config
git clone https://gitlab.com/yuval256/vim.git ~/.config/nvim

# install Plug
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
nvim -c PlugInstall -c qa

# install coc extentions
nvim -c 'CocInstall -sync coc-explorer coc-json coc-yaml coc-python' -c qa
